import React from "react";

import { ItemReceipt } from "views/ItemReceipt";
/*
ItemReceipt is a component however i'm curious on the reasoning why it's under a views folder
*/
function App() {
  return <ItemReceipt />;
}

export default App;
