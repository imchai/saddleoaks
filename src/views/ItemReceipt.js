import React, { useState } from "react";

import { TransactionHeader } from "components/TransactionHeader";
import { TransactionBody } from "components/TransactionBody";
import { TransactionFooter } from "components/TransactionFooter";

// Props isn't necessary here since we aren't receiving anything from App
export function ItemReceipt(props) {
  // I would rename lines and setLines to something more meaningful such as items and setItems as reading lines and setLines here confused me.
  const [lines, setLines] = useState([]);
  const [values, setValues] = useState({ description: "", amount: 0 });

  /*
  linesTotal, onChange, addItem are using all using var which is an anti pattern now since ES6+ is out.
  We should change all vars -> const or let since they are block scoped and will reduce bugs later.
  Also linesTotal function can be shortened into by using the array reduce method since we are just totaling the amount of the receipts items up.
  */
  function linesTotal(lines) {
    console.log("linesTotal, lines: %o", lines);
    var total = 0;
    for (var i = 0; i < lines.length; i++) {
      total = total + lines[i].amount;
    }
    return total;
  }

  function onChange(e) {
    var newValues = { ...values, [e.target.id]: e.target.value };
    setValues(newValues);
  }
  /*
  newLines pushing the amount value should be converted into a number since it's concatenating the strings onto the DOM.
  We should clear the fields here after the user submits the new items.
  */
  function addItem() {
    var newLines = [...lines];
    newLines.push({ description: values.description, amount: values.amount });
    setLines(newLines);
  }
  /*
  The form should be the only event delegator, so we should move the addItem function from the button element into the onSubmit prop below preventDefault.
  Also add type="submit" to the button prop to fire off the onSubmit event.

  The input forms should have a "name" property to define the input rather than id as id's are used only for unique distinction for CSS styling.

  In my opinion, the transaction components should be combined since they are such tiny components and we are passing in lines 2x as props into body and footer.
  */
  return (
    <>
      <TransactionHeader>Item receipt</TransactionHeader>
      <TransactionBody lines={lines} />
      <TransactionFooter total={linesTotal(lines)} />
      <hr />
      <form style={{ width: "25em" }} onSubmit={(e) => e.preventDefault()}>
        <fieldset>
          <legend>Add a line item</legend>
          <p>
            <label htmlFor="description">Description </label>
            <input
              id="description"
              value={values.description}
              onChange={onChange}
            />
          </p>
          <p>
            <label htmlFor="amount">Amount </label>
            <input id="amount" value={values.amount} onChange={onChange} />
          </p>
          <p>
            <button onClick={addItem}>Add</button>
          </p>
        </fieldset>
      </form>
    </>
  );
}
