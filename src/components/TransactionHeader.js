import React from "react";

export function TransactionHeader(props) {
  return <h1>{props.children}</h1>;
}
