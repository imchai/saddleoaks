import React from "react";

/*
There's a bug in the key prop here.
If a user purchases multiple items of the same description, React will render the same description key for both of those items.
To fix this, we can add a index value beside the description or have the server create unique IDs for each purchase.
*/
export function TransactionBody(props) {
  return (
    <>
      <table>
        <tbody>
          {props.lines.map((line) => (
            <tr key={line.description}>
              <td>{line.description}</td>
              <td>{line.amount}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
